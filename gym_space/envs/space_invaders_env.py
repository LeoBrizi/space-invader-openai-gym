import numpy as np
import time
import gym
from gym import error, spaces
from gym import utils
import random

ALIEN = 'X'
AGENT = 'A'
AGENT_SHOT = '*'
ALIEN_SHOT = 'o'
ALIEN_PLUS_SHOT = 'W'
GRID_DIM = 9

class SpaceInvEnv(gym.Env):
    """
    Description:
        Simplified version of the atari game Space invaders. With a reduce state space and the only thing which is not static is the agent, the rest is static.
    Observation: 
        my position, alien shot position, agent shot position   
    Actions:
        Num Action
        0   stay still
        1   move to the right
        2   move to the left
        3   shot
    Reward:
        100 for each kill, -500 for each losing life, -1 for each move
    Starting State:
        alien 
    Episode Termination:
        1.all aliens are dead
        2.lives == 0
    """
    def __init__(self):
        self.actionSpace = spaces.Discrete(4)
        self.actionSet = { 0 : "STILL", 1 : "RIGHT" , 2 : "LEFT" , 3 : "SHOT"}
        self.__reward = 0
        self.__lives = 3
        
        self.__state = np.matrix(
                        [["-", "-", "-", "-", "-", "-", "-", "-", "-"],
                         ["-", "X", "-", "X", "-", "X", "-", "X", "-"],
                         ["-", "-", "-", "-", "-", "-", "-", "-", "-"],
                         ["-", "X", "-", "X", "-", "X", "-", "X", "-"],
                         ["-", "-", "-", "-", "-", "-", "-", "-", "-"],
                         ["-", "X", "-", "X", "-", "X", "-", "X", "-"],
                         ["-", "-", "-", "-", "-", "-", "-", "-", "-"],
                         ["-", "-", "-", "-", "-", "-", "-", "-", "-"],
                         ["-", "-", "-", "-", "A", "-", "-", "-", "-"]]
                        )
        self.__aliens = [(1,1),(1,3),(1,5),(1,7),
                        (3,1),(3,3),(3,5),(3,7),
                        (5,1),(5,3),(5,5),(5,7)]
        '''
        self.__state = np.matrix(
                        [["-", "-", "-", "-", "-", "-", "-", "-", "-"],
                         ["-", "X", "-", "X", "-", "X", "-", "X", "-"],
                         ["-", "-", "X", "-", "X", "-", "X", "-", "-"],
                         ["-", "-", "-", "-", "-", "-", "-", "-", "-"],
                         ["-", "-", "X", "-", "X", "-", "X", "-", "-"],
                         ["-", "X", "-", "X", "-", "X", "-", "X", "-"],
                         ["-", "-", "-", "-", "-", "-", "-", "-", "-"],
                         ["-", "-", "-", "-", "-", "-", "-", "-", "-"],
                         ["-", "-", "-", "-", "A", "-", "-", "-", "-"]]
                        )
        self.__aliens = [(1,1),(1,3),(1,5),(1,7),
                        (2,2),(2,4),(2,6),
                        #(3,1),(3,3),(3,5),(3,7),
                        (4,2),(4,4),(4,6),
                        (5,1),(5,3),(5,5),(5,7)]
        '''
        self.__agent = int(GRID_DIM/2)
        self.__alienShot = (-1,-1)#(4,4)
        self.__agentShot = (-1,-1)#(7,4)
        self.__observation = {'agentPosition' : self.__agent, 'alienShotPos' : self.__alienShot, 'agentShotPos' : self.__agentShot}
        self.__utility = {}


    # choose a random action from the action set
    def choose_an_action(self):
        return self.actionSet[np.random.randint(0, 4)]

    # return my position on the last row
    def get_agent(self):
        return self.__agent

    # print an image of the state
    def render(self):
         print(self.__state)
         print("\n")

    # it return the current observation 
    def get_obs(self):
       return self.__observation

    def __alien_shot(self):
        if(len(self.__aliens) == 0): return
        alienNumber = np.random.randint(0, len(self.__aliens))
        shootingAlien = self.__aliens[alienNumber]
        self.__alienShot = (shootingAlien[0] +1, shootingAlien[1])
        if(self.__state[self.__alienShot] == ALIEN):
            self.__state[self.__alienShot] = ALIEN_PLUS_SHOT

        elif(self.__state[self.__alienShot] == AGENT_SHOT):
            # 80% gli spari si annullano, al 20% vince quello alieno
            #prob = random.uniform(0, 1)
            #if(prob >= 0.8):
            #    self.__agentShot = (-1, -1)
            #    self.__state[self.__alienShot] = ALIEN_SHOT
            #else:
            self.__state[self.__alienShot] = '-'
            self.__alienShot = (-1, -1)
            self.__agentShot = (-1, -1)

        else:
            self.__state[self.__alienShot] = ALIEN_SHOT

    def __move_alien_shot(self):
        #cancella il vecchio
        if(self.__state[self.__alienShot] == ALIEN_PLUS_SHOT):
            self.__state[self.__alienShot] = ALIEN

        else:
            self.__state[self.__alienShot] = '-'

        if(self.__alienShot[0] == GRID_DIM-1):
            self.__alienShot = (-1, -1)
            return

        self.__alienShot = (self.__alienShot[0] +1, self.__alienShot[1])
        #scrivi nuova posizione
        if(self.__state[self.__alienShot] == ALIEN):
            self.__state[self.__alienShot] = ALIEN_PLUS_SHOT

        elif(self.__state[self.__alienShot] == AGENT):
            self.__lives -= 1
            self.__alienShot = (-1, -1)
            self.__reward -= 500

        elif(self.__state[self.__alienShot] == AGENT_SHOT):
            # 80% gli spari si annullano, al 20% vince quello alieno
            #prob = random.uniform(0, 1)
            #if(prob >= 0.8):
            #    self.__agentShot = (-1, -1)
            #    self.__state[self.__alienShot] = ALIEN_SHOT
            #else:
            self.__state[self.__alienShot] = '-'
            self.__alienShot = (-1, -1)
            self.__agentShot = (-1, -1)

        else:
            self.__state[self.__alienShot] = ALIEN_SHOT

    def __agent_shot(self):
        self.__agentShot = (GRID_DIM -2, self.__agent)
        if(self.__state[self.__agentShot] == ALIEN_SHOT):
            # 80% gli spari si annullano, al 20% vince quello alieno
            #prob = random.uniform(0, 1)
            #if(prob >= 0.8):
            #    self.__agentShot = (-1, -1)
            #    self.__state[self.__alienShot] = ALIEN_SHOT

            #else:
            self.__state[self.__alienShot] = '-'
            self.__alienShot = (-1, -1)
            self.__agentShot = (-1, -1)

        else:
            self.__state[self.__agentShot] = AGENT_SHOT


    def __move_agent_shot(self):
        #cancella il vecchio
        self.__state[self.__agentShot] = '-'
        if(self.__agentShot[0] == 0):
            self.__agentShot = (-1, -1)
            return

        self.__agentShot = (self.__agentShot[0]-1, self.__agentShot[1])

        if(self.__state[self.__agentShot] == ALIEN):
            self.__reward += 100
            self.__aliens.remove(self.__agentShot)
            self.__state[self.__agentShot] = '-'
            self.__agentShot = (-1, -1)

        elif(self.__state[self.__agentShot] == ALIEN_SHOT):
            # 80% gli spari si annullano, al 20% vince quello alieno
            prob = random.uniform(0, 1)
            #if(prob >= 0.8):
            #    self.__agentShot = (-1, -1)
            #    self.__state[self.__agentShot] = ALIEN_SHOT

            #else:
            self.__state[self.__agentShot] = '-'
            self.__alienShot = (-1, -1)
            self.__agentShot = (-1, -1)

        else:
            self.__state[self.__agentShot] = AGENT_SHOT

             
    #return observation(object), reward(float), done(boolean),info
    def step(self, action):
        prob = random.uniform(0, 1)
        alienCanShot = True if self.__alienShot[0] == -1 else False
        agentCanShot = True if self.__agentShot[0] == -1 else False
        alienHShot = False
        agentHShot = False
        if(alienCanShot):
            self.__alien_shot()
            alienHShot = True

        if(action == 'STILL'):
            self.__reward -= 1
        if(action == 'RIGHT'):
            self.__reward -= 1
            if(prob <= 0.9 and self.__agent != GRID_DIM-1):
                self.__state[GRID_DIM-1, self.__agent] = '-'
                self.__agent += 1
                self.__state[GRID_DIM-1, self.__agent] = AGENT

        elif(action == 'LEFT'):
            self.__reward -= 1
            if(prob <= 0.9 and self.__agent != 0):
                self.__state[GRID_DIM-1, self.__agent] = '-'
                self.__agent -= 1
                self.__state[GRID_DIM-1, self.__agent] = AGENT

        elif(action == 'SHOT'):
            self.__reward -= 1
            if(agentCanShot):
                self.__agent_shot()
                agentHShot = True

        if(self.__agentShot[0] != -1 and not(agentHShot)):
            self.__move_agent_shot()
        if(self.__alienShot[0] != -1 and not(alienHShot)):
            self.__move_alien_shot()
        self.__observation = {'agentPosition' : self.__agent, 'alienShotPos' : self.__alienShot, 'agentShotPos' : self.__agentShot}
        done = True if(len(self.__aliens) == 0 or self.__lives == 0) else False
        return self.__observation, self.__reward, done, self.__state

    # return: (states, observations)
    def reset(self):
        self.__reward = 0
        self.__lives = 3
        
        self.__state = np.matrix(
                        [["-", "-", "-", "-", "-", "-", "-", "-", "-"],
                         ["-", "X", "-", "X", "-", "X", "-", "X", "-"],
                         ["-", "-", "-", "-", "-", "-", "-", "-", "-"],
                         ["-", "X", "-", "X", "-", "X", "-", "X", "-"],
                         ["-", "-", "-", "-", "-", "-", "-", "-", "-"],
                         ["-", "X", "-", "X", "-", "X", "-", "X", "-"],
                         ["-", "-", "-", "-", "-", "-", "-", "-", "-"],
                         ["-", "-", "-", "-", "-", "-", "-", "-", "-"],
                         ["-", "-", "-", "-", "A", "-", "-", "-", "-"]]
                        )
        self.__aliens = [(1,1),(1,3),(1,5),(1,7),
                        (3,1),(3,3),(3,5),(3,7),
                        (5,1),(5,3),(5,5),(5,7)]
        '''
        self.__state = np.matrix(
                        [["-", "-", "-", "-", "-", "-", "-", "-", "-"],
                         ["-", "X", "-", "X", "-", "X", "-", "X", "-"],
                         ["-", "-", "X", "-", "X", "-", "X", "-", "-"],
                         ["-", "-", "-", "-", "-", "-", "-", "X", "-"],
                         ["-", "-", "X", "-", "X", "-", "X", "-", "-"],
                         ["-", "X", "-", "X", "-", "X", "-", "-", "-"],
                         ["-", "-", "-", "-", "-", "-", "-", "-", "-"],
                         ["-", "-", "-", "-", "-", "-", "-", "-", "-"],
                         ["-", "-", "-", "-", "A", "-", "-", "-", "-"]]
                        )
        self.__aliens = [(1,1),(1,3),(1,5),(1,7),
                        (2,2),(2,4),(2,6),
                        #(3,1),(3,3),(3,5),
                        (3,7),
                        (4,2),(4,4),(4,6),
                        (5,1),(5,3),(5,5)]#,(5,7)]
        '''
        self.__agent = int(GRID_DIM/2)
        self.__alienShot = (-1,-1)#(4,4)
        self.__agentShot = (-1,-1)#(7,4)
        self.__observation = {'agentPosition' : self.__agent, 'alienShotPos' : self.__alienShot, 'agentShotPos' : self.__agentShot}
        return self.__state, self.__observation

    def __reward_function(self, state):
        agentPos = int(state[0])
        roba = ""
        i = 1
        while i < len(state):
            if(state[i] == '-'):
                roba = roba+state[i]+state[i+1]+'+'
                i +=2
            else:
                roba = roba+state[i]+'+'
                i += 1
        robetta = roba.split('+')
        rAgentShot = int(robetta[0]) 
        cAgentShot = int(robetta[1]) 
        rAlienShot = int(robetta[2]) 
        cAlienShot = int(robetta[3]) 

        reward = 0
        if(agentPos == cAlienShot and rAlienShot == 8): #ho perso una vita
            reward -= 500
        if((rAgentShot, cAgentShot) in self.__aliens): #ho ucciso un alieno
            reward += 100
        else:
            reward -= 1 #gli altri stati
        return reward

    def __best_can_do_in_future(self, state):
        agentPos = int(state[0])
        roba = ""
        i = 1
        while i < len(state):
            if(state[i] == '-'):
                roba = roba+state[i]+state[i+1]+'+'
                i +=2
            else:
                roba = roba+state[i]+'+'
                i += 1
        robetta = roba.split('+')
        rAgentShot = int(robetta[0]) 
        cAgentShot = int(robetta[1]) 
        rAlienShot = int(robetta[2]) 
        cAlienShot = int(robetta[3]) 

        nextRAgentShot = rAgentShot
        nextCAgentShot = cAgentShot
        if(rAgentShot == 0):
            nextRAgentShot = -1
            nextCAgentShot = -1
        elif(rAgentShot != -1):
            nextRAgentShot -= 1 

        nextRAlienShot = rAlienShot
        nextCAlienShot = cAlienShot 
        if(rAlienShot == GRID_DIM-1):
            nextRAlienShot = -1
            nextCAlienShot = -1
        elif(rAlienShot != 1):
            nextRAlienShot += 1 

        if(rAgentShot == rAlienShot and cAgentShot == cAlienShot):
            nextRAgentShot = -1
            nextCAgentShot = -1
            nextRAlienShot = -1
            nextCAlienShot = -1

        nextStateStill = str(agentPos)+str(nextRAgentShot)+str(nextCAgentShot)+str(nextRAlienShot)+str(nextCAlienShot)
        nextStateShot = nextStateStill if(rAgentShot != -1) else str(agentPos)+str(GRID_DIM -2)+str(agentPos)+str(nextRAlienShot)+str(nextCAlienShot)
        
        nextAgentPosLeft = agentPos -1 if(agentPos != 0) else agentPos
        nextAgentPosRight = agentPos +1 if(agentPos != GRID_DIM-1) else agentPos

        nextStateLeft = str(nextAgentPosLeft)+str(nextRAgentShot)+str(nextCAgentShot)+str(nextRAlienShot)+str(nextCAlienShot)
        nextStateRight = str(nextAgentPosRight)+str(nextRAgentShot)+str(nextCAgentShot)+str(nextRAlienShot)+str(nextCAlienShot)

        doingShot = 1*self.__utility[nextStateShot]
        doingStill = 1*self.__utility[nextStateStill]
        doingLeft = 0.1*self.__utility[nextStateStill] + 0.9*self.__utility[nextStateLeft]
        doingRight = 0.1*self.__utility[nextStateStill] + 0.9*self.__utility[nextStateRight]
        result = np.array([doingShot, doingStill, doingLeft, doingRight])
        return max(result)


    #my state is agent position, agent_shot position e alien shot position
    def value_iteration(self,epsilon):
        gamma = 0.9
        if(len(self.__utility.keys()) == 0):
            for agentPos in range(9): #all the possible value for agentPos
                for rAgentShot in range(-1,8):
                    for cAgentShot in range(-1,9):
                        for rAlienShot in range(-1,9):
                            for cAlienShot in range(-1,8):
                                state = str(agentPos)+str(rAgentShot)+str(cAgentShot)+str(rAlienShot)+str(cAlienShot)
                                self.__utility[state] = 0
        Uprime = self.__utility.copy()
        iteration = 0
        while(True):
            iteration += 1
            self.__utility = Uprime.copy()
            delta = 0 #maximum change in the utility of any state in a iteration
            for state in self.__utility.keys():
                Uprime[state] = self.__reward_function(state) + gamma* self.__best_can_do_in_future(state)
                if(abs(Uprime[state] - self.__utility[state]) > delta):
                    delta = abs(Uprime[state] - self.__utility[state])
            if(delta < epsilon*(1-delta)/delta):
                break;
        return self.__utility, iteration

    def choose_best_action(self, state):
        agentPos = int(state[0])
        roba = ""
        i = 1
        while i < len(state):
            if(state[i] == '-'):
                roba = roba+state[i]+state[i+1]+'+'
                i +=2
            else:
                roba = roba+state[i]+'+'
                i += 1
        robetta = roba.split('+')
        rAgentShot = int(robetta[0]) 
        cAgentShot = int(robetta[1]) 
        rAlienShot = int(robetta[2]) 
        cAlienShot = int(robetta[3]) 

        nextRAgentShot = rAgentShot
        nextCAgentShot = cAgentShot
        if(rAgentShot == 0):
            nextRAgentShot = -1
            nextCAgentShot = -1
        elif(rAgentShot != -1):
            nextRAgentShot -= 1 

        nextRAlienShot = rAlienShot
        nextCAlienShot = cAlienShot 
        if(rAlienShot == GRID_DIM-1):
            nextRAlienShot = -1
            nextCAlienShot = -1
        elif(rAlienShot != 1):
            nextRAlienShot += 1 

        if(rAgentShot == rAlienShot and cAgentShot == cAlienShot):
            nextRAgentShot = -1
            nextCAgentShot = -1
            nextRAlienShot = -1
            nextCAlienShot = -1

        nextStateStill = str(agentPos)+str(nextRAgentShot)+str(nextCAgentShot)+str(nextRAlienShot)+str(nextCAlienShot)
        nextStateShot = nextStateStill if(rAgentShot != -1) else str(agentPos)+str(GRID_DIM -2)+str(agentPos)+str(nextRAlienShot)+str(nextCAlienShot)
        
        nextAgentPosLeft = agentPos -1 if(agentPos != 0) else agentPos
        nextAgentPosRight = agentPos +1 if(agentPos != GRID_DIM-1) else agentPos

        nextStateLeft = str(nextAgentPosLeft)+str(nextRAgentShot)+str(nextCAgentShot)+str(nextRAlienShot)+str(nextCAlienShot)
        nextStateRight = str(nextAgentPosRight)+str(nextRAgentShot)+str(nextCAgentShot)+str(nextRAlienShot)+str(nextCAlienShot)

        doingShot = 1*self.__utility[nextStateShot]
        doingStill = 1*self.__utility[nextStateStill]
        doingLeft = 0.1*self.__utility[nextStateStill] + 0.9*self.__utility[nextStateLeft]
        doingRight = 0.1*self.__utility[nextStateStill] + 0.9*self.__utility[nextStateRight]
        result = [doingShot, doingStill, doingLeft, doingRight]
        bestAction = result.index(max(result))
        po = 'RIGHT'
        if(bestAction == 0):
            po = 'SHOT'
        elif(bestAction == 1):
            po = 'STILL'
        elif(bestAction == 2):
            po = 'LEFT'
        return po
