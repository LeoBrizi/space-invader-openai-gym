import gym_space 
import gym 
import random


env = gym.make('MySpaceInvaders-v0')
state, observation = env.reset()
env.render()
print("initial observation: ")
print(observation)
for _ in range(10):
	print("NUOVO STEP")
	action = env.choose_an_action()
	observation, reward, done, state = env.step(action)
	env.render()
	print("observation:")
	print(observation)
	print("action: ")
	print(action)
	print("reward:")
	print(reward)
	print("done: ")
	print(done)

risultato, it = env.value_iteration(0.1)
print(risultato)
print("iterazioni", it)

try:
    input("Press enter to continue")
except SyntaxError:
    pass

env.reset()
nonFinito = True
action = 'STILL'
count = 0
while(nonFinito):
	if(count == 10):
		env.value_iteration(0.1)
		count = 0
	#prob = random.uniform(0, 1)
	#randomAction = env.choose_an_action()
	#if(prob > 0.8):
	#	action = randomAction
	observation, reward, done, state = env.step(action)
	env.render()
	nonFinito = not(done)
	s = str(observation['agentPosition'])+str(observation['agentShotPos'][0])+str(observation['agentShotPos'][1])+str(observation['alienShotPos'][0])+str(observation['alienShotPos'][1])
	action = env.choose_best_action(s)
	count += 1